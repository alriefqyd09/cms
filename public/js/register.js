// require('jquery.min');

(function ($) {
    $(function() {
        var register_form = $('.form-register');
        var email = register_form.find('.js-email-validation');

        $.validator.addMethod("validEmail", function (value, element, params) {
            if (/^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?(fairtech)\.com\.sg$/.test(value)) {
                return true;
            } else {
                return false;
            }
        });

        register_form.validate({
            rules:{
                email:{
                    validEmail:true,
                    email:true
                }
            },
            messages:{
                email:{
                    validEmail:email.data('valid-email-messages')
                }
            }
        })
    });
})(jQuery);

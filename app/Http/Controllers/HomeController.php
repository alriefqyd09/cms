<?php

namespace App\Http\Controllers;

use Bjuppa\LaravelBlog\Eloquent\BlogEntry;
use Bjuppa\LaravelBlog\Http\Controllers\BaseBlogController;
use Bjuppa\LaravelBlog\Http\Controllers\ListEntriesController;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController extends BaseBlogController
{
    protected function index(){
        $artikel = BlogEntry::limit(3)->get();
        return view("welcome")->with('artikel',$artikel);
    }
}
